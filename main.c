#include "sam.h"
#include "system_samd21.h"

// The LED is connected to port A pin 17.
#define LED_BITMASK 1<<17

void main(void)
{
    uint32_t i;

    SystemInit();

    REG_PORT_DIR0 = LED_BITMASK;
    REG_PORT_OUT0 = LED_BITMASK;

    while(1)
    {
        for(i=0; i!=0x7FFF; i++);
        REG_PORT_OUT0 ^= LED_BITMASK;
    }
}
