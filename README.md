# Adafruit Feather M0 Basic Proto Barebones Makefile Project

This is a minimalist project to get people started developing on the Adafruit 
Feather M0 Basic Proto platform without the Arduino IDE. It is aimed at gcc C99, 
though it could be easily modified to support different C versions or C++.  

If you like the Adafruit Feather hardware, but want a more flexible build 
environment than the Arduino IDE provides, or you're curious about the GCC
magic going on under the hood of the Arduino IDE then I hope you'll find this
project useful.

This project uses the following GCC tools:
* arm-none-eabi-gcc
* arm-none-eabi-newlib
* arm-none-eabi-binutils  
If you are running Linux there is a good chance these are available from your 
distribution's repositories.  

This project depends on Arm CMSIS (Cortex Microcontroller Software Interface 
Standard). The recommended way of installing downloading CMSIS is to navigate
to the root of the project folder and run 
'git clone https://github.com/ARM-software/CMSIS.git'.  

This project uses the bootloader that ships on the Feather. To communicate with
the bootloader install [BOSSA](http://www.shumatech.com/web/products/bossa).  

## Debian/Ubuntu Instructions

Install the necessary packages.
```
apt install make gcc-arm-none-eabi binutils-arm-none-eabi libnewlib-arm-none-eabi bossa-cli git  
```  
Clone this repository.
```
git clone git@gitlab.com:evanpower/adafruit-feather-barebones-makefile-project.git  
```  
Navigate into the project directory.
```  
cd adafruit-barebones-makefile-project  
```
Clone the CMSIS repository.
```  
git clone https://github.com/ARM-software/CMSIS.git  
```  
Build the project.
```
make
```  
Write it to the Feather. Double-press the reset button on the Feather. Its LED will begin to pulse to 
show that it's in bootloader mode.  
```
make flash
```  

Note: your operating system may assign a different port name to the 
bootloader's virtual serial port. On Linux I recommend running 'ls /dev' in a
terminal with the Feather disconnected fro the computer, and then connecting
the Feather to the one of the computer's USB ports, putting it into bootloader
mode (by double-pressing the reset button) and then running 'ls /dev' again. 
There should be one entry that was not there the first time. That is the name
of the bootloader's virtual serial port. Set the PORTNAME variable in the 
makefile to that name.

Another note: There may be permissions problems accessing the virtual serial 
port. See this post: 
https://superuser.com/questions/1195006/linux-connecting-to-a-usb-virtual-com-port-without-root-access
 
 Thanks to the following for making this project possible:
 * [The GCC Project](https://gcc.gnu.org/)
 * [Adafruit Industries](https://www.adafruit.com/)
 * [Arduino](https://www.arduino.cc)
 * [ShumaTech (BOSSA)](http://www.shumatech.com/web/products/bossa)
 * [MickMake](https://www.mickmake.com/post/program-atmel-sam-without-ide-tutorial)
