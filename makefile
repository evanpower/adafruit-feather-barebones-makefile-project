# This will  be the name of your output files.
BIN= feathertest
# Put the name of the Feather's bootloader serial port here.
PORTNAME= ttyACM0
# Put the path to the CMSIS include folder here.
CMSIS_INCLUDE= "./CMSIS/CMSIS/Include/"

SOURCES= main.c
SOURCES+= ./samd21a/gcc/gcc/startup_samd21.c
SOURCES+= ./samd21a/gcc/system_samd21.c

OUTPUTDIR= ./output
OBJDIR= ./obj
OBJECTS= $(patsubst %.c,$(OBJDIR)/%.o,$(SOURCES))
LINKERSCRIPT= ./samd21a/gcc/gcc/samd21g18a_flash.ld

# Depending on your operating system you may have to modify these variables.
# These are the compiler, binutils and linker.
CC= arm-none-eabi-gcc
OBJCOPY= arm-none-eabi-objcopy
LD=arm-none-eabi-gcc
DBG=arm-none-eabi-gdb

CFLAGS= -mcpu=cortex-m0plus -mthumb -c -std=c99
CFLAGS+= -D__SAMD21G18A__
CFLAGS+= -DDONT_USE_CMSIS_INIT
CFLAGS+= -I"./samd21a/include/"
CFLAGS+= -I ${CMSIS_INCLUDE}
CFLAGS+= -g

LDFLAGS= -mcpu=cortex-m0plus -mthumb
LDFLAGS+= -Wl,--gc-sections
LDFLAGS+= -Wl,--script=${LINKERSCRIPT}

DEBUGFLAGS= -tui
DEBUGFLAGS+= --directory=.

# By default make a bin file.
all: ${OUTPUTDIR}/${BIN}.bin

# If you need the bin file, first you'll need the elf file.
${OUTPUTDIR}/${BIN}.bin: ${OUTPUTDIR}/${BIN}.elf
	${OBJCOPY} -O binary ${OUTPUTDIR}/${BIN}.elf ${OUTPUTDIR}/${BIN}.bin

# If you need an elf file, first you'll need all the object files.
# You'll also need to have the linker script.
${OUTPUTDIR}/${BIN}.elf: ${OBJECTS} ${LINKERSCRIPT}
	mkdir -p $(@D)
	${LD} ${LDFLAGS} $^ -o $@

# If you need an object file, first you'll need a c file of the same name.
${OBJDIR}/%.o: %.c
	mkdir -p $(@D)
	${CC} -Os ${CFLAGS} -c -o $@ $<

.PHONY: flash clean debug

# Use BOSSA to write thr program to the device. First you will need the .bin file.
flash: ${OUTPUTDIR}/${BIN}.bin
	bossac -i -d --port=${PORTNAME} -U -e -w -v ${OUTPUTDIR}/${BIN}.bin -R

# Debug with a Black Magic Probe. First you will need the .elf file. The serial port name that the OS gives the
# Black Magic Probe will have to be specified in .gdbinit file in this folder.
# (TODO make this better)
debug:
	${DBG} ${DEBUGFLAGS} ${OUTPUTDIR}/${BIN}.elf

clean:
	rm -rf ${OUTPUTDIR}
	rm -rf ${OBJDIR}
